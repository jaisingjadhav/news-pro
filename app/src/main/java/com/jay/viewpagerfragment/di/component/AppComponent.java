package com.jay.viewpagerfragment.di.component;

import android.support.v4.app.Fragment;

import com.jay.viewpagerfragment.di.modules.AppModule;
import com.jay.viewpagerfragment.di.modules.UtilsModule;
import com.jay.viewpagerfragment.ui.activities.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class, UtilsModule.class})
@Singleton
public interface AppComponent {

    void doInjection(MainActivity mainActivity);
    void doInjection(Fragment fragment);

}