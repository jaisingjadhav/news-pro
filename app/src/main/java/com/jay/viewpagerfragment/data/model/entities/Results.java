package com.jay.viewpagerfragment.data.model.entities;

import android.arch.persistence.room.Relation;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Results {
    int id;


    String title;
    String section;
    String subsection;
    @SerializedName("abstract")
    String short_abstract;
    String url;
    String byline;
    String item_type;
    String updated_date;
    String created_date;
    String published_date;
    String short_url;

    @Relation(parentColumn = "id", entityColumn = "article_id")
    public List<Multimedia> multimedia;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSubsection() {
        return subsection;
    }

    public void setSubsection(String subsection) {
        this.subsection = subsection;
    }

    public String getShort_abstract() {
        return short_abstract;
    }

    public void setShort_abstract(String short_abstract) {
        this.short_abstract = short_abstract;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getByline() {
        return byline;
    }

    public void setByline(String byline) {
        this.byline = byline;
    }

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getPublished_date() {
        return published_date;
    }

    public void setPublished_date(String published_date) {
        this.published_date = published_date;
    }

    public String getShort_url() {
        return short_url;
    }

    public void setShort_url(String short_url) {
        this.short_url = short_url;
    }

    public List<Multimedia> getMultimediaList() {
        return multimedia;
    }

    public void setMultimediaList(List<Multimedia> multimediaList) {
        this.multimedia = multimediaList;
    }
}
