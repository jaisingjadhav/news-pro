package com.jay.viewpagerfragment.data.repo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import com.google.gson.JsonElement;
import com.jay.viewpagerfragment.R;
import com.jay.viewpagerfragment.data.model.dao.NewsDao;
import com.jay.viewpagerfragment.data.model.db.NewsDatabase;
import com.jay.viewpagerfragment.data.model.entities.Multimedia;
import com.jay.viewpagerfragment.data.model.entities.NewsArticle;
import com.jay.viewpagerfragment.data.model.entities.NewsSection;
import com.jay.viewpagerfragment.data.rest.ApiCallInterface;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

public class Repository {

    private ApiCallInterface apiCallInterface;
    private Context context;
    private NewsDao newsDao;
    public Repository(ApiCallInterface apiCallInterface, Context context) {
        this.apiCallInterface = apiCallInterface;
        this.context =context;
        NewsDatabase noteDatabase = NewsDatabase.getInstance(context);
        newsDao = noteDatabase.newsDao();
    }


    public List<NewsSection> getNewsSectionFromLocal(){
        String[] section_array = context.getResources().getStringArray(R.array.sections);
        List<NewsSection> newsSections = null;
        if (section_array !=null && section_array.length>0){
            newsSections = new ArrayList<>(section_array.length);

            for (String s : section_array){
                newsSections.add(new NewsSection(s, s.toLowerCase().replace(" ", ""), 0));
            }
        }
        return newsSections;
    }


    public LiveData<List<NewsSection>> getNewsSections(){
        return newsDao.getAllSections();
    }

    public Single<NewsSection> getFirstNewsSection(){
        return Single.create(new SingleOnSubscribe<NewsSection>() {
            @Override
            public void subscribe(SingleEmitter<NewsSection> emitter) throws Exception {
                try {
                    emitter.onSuccess(newsDao.getActiveNewsSection());
                }catch (Exception e){
                    emitter.onError(e);
                }
            }
        });
    }

    public Observable<JsonElement> getAllArticle(String section){
        return apiCallInterface.getArticles(section);
    }

    public int updateNewsSection(NewsSection newsSection){
        return newsDao.updateSection(newsSection);
    }

    public boolean deleteNewsSection(NewsSection newsSection){
        return true;
    }

    public boolean addNewsSection(NewsSection newsSection){
        return true;
    }

    public boolean updateNewsArticle(NewsArticle newsArticle){
        return true;
    }

    public boolean deleteNewsArticle(NewsArticle newsArticle){
        return true;
    }

    public LiveData<List<NewsArticle>> getNewsArticles(int sectionId){
        return newsDao.getNewsArticles(sectionId);
    }

    public long addNewsArticle(List<NewsArticle> newsArticleList, int sectionId){
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                for (NewsArticle newsArticle: newsArticleList){
                    newsArticle.setSection_id(sectionId);
                    List<Multimedia> multimedia = newsArticle.getMultimedia();
                    long articleId = 0;
                    if (multimedia !=null && multimedia.size() > 0)
                        newsArticle.setImage_url(multimedia.get(0).getUrl());
                    try {
                        articleId = newsDao.insertArtice(newsArticle);
                    }catch (Exception e){
                        Log.e("INsertion ", e.getMessage());
                    }
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        Log.v("Data added", "true");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
        return 1;
    }

    public boolean updateNewsMedia(Multimedia multimedia){
        return true;
    }
    public boolean deleteNewsMedia(Multimedia multimedia){
        return true;
    }
    public boolean addNewsMedia(Multimedia multimedia){
        return true;
    }

    public void clearArticles(int sectionId){
        newsDao.deleteAllArticles(sectionId);
    }
}
