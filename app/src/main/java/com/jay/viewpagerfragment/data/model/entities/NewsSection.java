package com.jay.viewpagerfragment.data.model.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "news_section", indices = {@Index(value = {"displayName"}, unique = true)})
public class NewsSection {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String displayName;
    private String sectionName;
    private int status = 0;

    public NewsSection(String displayName, String sectionName, int status) {
        this.displayName = displayName;
        this.sectionName = sectionName;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
