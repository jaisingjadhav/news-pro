package com.jay.viewpagerfragment.data.rest;

public enum Status {
    LOADING,
    SUCCESS,
    ERROR,
    COMPLETED
}
