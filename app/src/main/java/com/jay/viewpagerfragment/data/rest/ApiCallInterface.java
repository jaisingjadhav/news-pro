package com.jay.viewpagerfragment.data.rest;

import com.google.gson.JsonElement;
import com.jay.viewpagerfragment.utils.UtilFunctions;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiCallInterface {

    @GET("svc/topstories/v2/{section}?api-key="+ UtilFunctions.API_KEY)
    Observable<JsonElement> getArticles(@Path("section") String section);
}
