package com.jay.viewpagerfragment.data.model.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "multimedia", foreignKeys = @ForeignKey(entity = NewsArticle.class,
        parentColumns = "id",
        childColumns = "article_id",
        onDelete = ForeignKey.CASCADE))
public class Multimedia {
    @PrimaryKey(autoGenerate = true)
    int id;
    String url;
    String type;
    String subtype;
    String caption;

    int article_id;


    public Multimedia(String url, String type, String subtype, String caption, int article_id) {
        this.url = url;
        this.type = type;
        this.subtype = subtype;
        this.caption = caption;
        this.article_id = article_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }
}
