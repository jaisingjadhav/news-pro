package com.jay.viewpagerfragment.data.model.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.jay.viewpagerfragment.data.model.entities.Results;
import com.jay.viewpagerfragment.data.model.entities.Multimedia;
import com.jay.viewpagerfragment.data.model.entities.NewsArticle;
import com.jay.viewpagerfragment.data.model.entities.NewsSection;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;

@Dao
public interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertSection(NewsSection newsSection);

    @Update
    int updateSection(NewsSection newsSection);

    @Delete
    int deleteSection(NewsSection newsSection);


    @Query("delete from news_section")
    void deleteAllSection();

    @Query("select * from news_section")
    LiveData<List<NewsSection>> getAllSections();

    @Query("select * from news_section where status > 0 limit 1")
    NewsSection getActiveNewsSection();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertArtice(NewsArticle newsArticle);

    @Update
    int updateArticle(NewsArticle newsArticle);

    @Delete
    int deleteArticle(NewsArticle newsArticle);


    @Query("delete from news_article where section_id=:sectionId")
    void deleteAllArticles(int sectionId);

//    @Query("select * from news_article/* as na left join Multimedia nm " +
//            "on na.id = nm.article_id order by na.published_date desc*/")
    @Query("select * from news_article where section_id =:sectionId")
    LiveData<List<NewsArticle>> getNewsArticles(int sectionId);

    @Insert
    long insertMultimedia(Multimedia multimedia);
}
