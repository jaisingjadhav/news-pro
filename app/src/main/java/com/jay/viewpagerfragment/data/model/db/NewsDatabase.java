package com.jay.viewpagerfragment.data.model.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.jay.viewpagerfragment.R;
import com.jay.viewpagerfragment.data.model.dao.NewsDao;
import com.jay.viewpagerfragment.data.model.entities.Multimedia;
import com.jay.viewpagerfragment.data.model.entities.NewsArticle;
import com.jay.viewpagerfragment.data.model.entities.NewsSection;

import java.util.ArrayList;
import java.util.List;

@Database(entities = {NewsSection.class, NewsArticle.class, Multimedia.class},
        version = 1, exportSchema = false)
public abstract class NewsDatabase extends RoomDatabase {

    private static NewsDatabase instance;

    static Context mContext;
    public abstract NewsDao newsDao();


    public static synchronized NewsDatabase getInstance(Context context) {
        mContext = context;
        if (instance == null)
            instance = Room.databaseBuilder(context.getApplicationContext(), NewsDatabase.class,
                    "promobi_news")
                    .addCallback(roomCallback)
                    .addMigrations()
                    .build();
        return instance;
    }

    static final Migration MIGRATION_1_2  = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE news_article" +
                    " "
                    + " ADD COLUMN section_id INTEGER NOT NULL");
        }
    };

    static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new AddNewsSection(instance).execute();
        }

    };

    private static class AddNewsSection extends AsyncTask<Void, Void, Void> {

        private NewsDao newsDao;
        AddNewsSection(NewsDatabase newsDatabase){
            this.newsDao = newsDatabase.newsDao();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            String[] section_array = mContext.getResources().getStringArray(R.array.sections);
            List<NewsSection> newsSections = null;
            if (section_array !=null && section_array.length>0){
                newsSections = new ArrayList<>(section_array.length);

                for (String s : section_array){
                    newsDao.insertSection(new NewsSection(s, s.toLowerCase().replace(" ", ""), 0));
                }
            }
            /*newsDao.insertSection(new NewsSection("Home", "home", 1));
            newsDao.insertSection(new NewsSection("World", "world", 1));
            newsDao.insertSection(new NewsSection("Arts", "arts", 1));
            newsDao.insertSection(new NewsSection("Technology", "technology", 1));*/
            return null;
        }
    }
}
