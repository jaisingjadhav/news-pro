package com.jay.viewpagerfragment.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

import com.jay.viewpagerfragment.events.MessageEvent;
import com.jay.viewpagerfragment.events.NetworkStatus;

import org.greenrobot.eventbus.EventBus;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isConnected() || mobile.isConnected()) {
            EventBus.getDefault().post(new MessageEvent(NetworkStatus.CONNECTED));

            Log.d("Network Available ", "Flag No 1");
        } else {
            EventBus.getDefault().post(new MessageEvent(NetworkStatus.DISCONNECTED));
        }
    }
}
