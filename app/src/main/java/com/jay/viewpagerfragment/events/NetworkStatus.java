package com.jay.viewpagerfragment.events;

public enum NetworkStatus {
    CONNECTING,
    CONNECTED,
    DISCONNECTED
}
