package com.jay.viewpagerfragment.events;

public class MessageEvent {
    public final NetworkStatus networkStatus;

    public MessageEvent(NetworkStatus networkStatus) {
        this.networkStatus = networkStatus;
    }

    public NetworkStatus getNetworkStatus() {
        return networkStatus;
    }
}
