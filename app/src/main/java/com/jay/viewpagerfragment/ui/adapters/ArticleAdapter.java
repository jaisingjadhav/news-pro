package com.jay.viewpagerfragment.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.jay.viewpagerfragment.R;
import com.jay.viewpagerfragment.data.model.entities.Multimedia;
import com.jay.viewpagerfragment.data.model.entities.NewsArticle;
import com.jay.viewpagerfragment.data.model.entities.NewsSection;
import com.jay.viewpagerfragment.ui.viewModels.ArticleViewModel;
import com.jay.viewpagerfragment.utils.UtilFunctions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArticleAdapter extends ListAdapter<NewsArticle, ArticleAdapter.ArticleViewHolder> {

    Context context;

    public ArticleAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;
    }

    private static final DiffUtil.ItemCallback<NewsArticle> DIFF_CALLBACK = new DiffUtil.ItemCallback<NewsArticle>() {
        @Override
        public boolean areItemsTheSame(@NonNull NewsArticle oldItem, @NonNull NewsArticle newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull NewsArticle oldItem, @NonNull NewsArticle newItem) {
            return oldItem.getTitle().equalsIgnoreCase(newItem.getTitle());
        }
    };
    OnClickListner onClickListner = null;

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClickListner = onClickListner;
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.article_list_item, viewGroup, false);
        return new ArticleViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder articleViewHolder, int i) {
        NewsArticle newsArticle = getItem(i);
        articleViewHolder.tv_title.setText(newsArticle.getTitle());
        articleViewHolder.tv_message.setText(newsArticle.getShort_abstract());
        articleViewHolder.tv_publishedBy.setText(newsArticle.getByline());
        String time = UtilFunctions.getReadableDate(newsArticle.getPublished_date());
        articleViewHolder.tv_time.setText(time);
        Glide.with(context)
                .load(newsArticle.getImage_url())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .centerCrop()
                .dontAnimate()
                .placeholder(R.mipmap.ic_launcher)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
                        articleViewHolder.imageView.startAnimation(animation);
                        return false;
                    }
                })
                .into(articleViewHolder.imageView);
    }


    public class ArticleViewHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.cardView)
//        CardView CardViewcv;

        @BindView(R.id.title)
        TextView tv_title;

        @BindView(R.id.tv_message)
        TextView tv_message;

        @BindView(R.id.tv_publishedBy)
        TextView tv_publishedBy;

        @BindView(R.id.tv_time)
        TextView tv_time;

        @BindView(R.id.imageView)
        ImageView imageView;

        @BindView(R.id.im_share)
        ImageView im_share;

        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
//            tv_title = itemView.findViewById(R.id.title);
//            tv_status = itemView.findViewById(R.id.tv_status);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v("onclick", getItem(getAdapterPosition()).getTitle());
                    if (onClickListner != null /*&& getItem(getAdapterPosition()).getStatus() > 0*/) {
                        onClickListner.onClick(ActionOnClick.VIEW, getItem(getAdapterPosition()));
                        //openWebPage(getItem(getAdapterPosition()).getShort_url());

                    }
                }
            });
        }

        @OnClick(R.id.im_share)
        public void onCLickShare(){
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL");
            i.putExtra(Intent.EXTRA_TEXT, getItem(getAdapterPosition()).getShort_url());
            context.startActivity(Intent.createChooser(i, "Share News"));
        }

        public void openWebPage(String url) {
            try {
                Uri webpage = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(intent);
                }else{
                    //Page not found
                }
            }catch (Exception e){
                Log.e("OpenWebPage", e.getMessage());
            }
        }
    }
}
