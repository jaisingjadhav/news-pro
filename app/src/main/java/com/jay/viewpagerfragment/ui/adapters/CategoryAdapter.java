package com.jay.viewpagerfragment.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jay.viewpagerfragment.R;
import com.jay.viewpagerfragment.data.model.entities.NewsSection;
import com.jay.viewpagerfragment.ui.viewModels.CatListViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryAdapter extends ListAdapter<NewsSection, CategoryAdapter.CategoryViewHolder> {

    Context context;
    public CategoryAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;
    }

    OnClickListner onClickListner = null;
    public void setOnClickListner(OnClickListner onClickListner){
        this.onClickListner = onClickListner;
    }

    private static final DiffUtil.ItemCallback<NewsSection> DIFF_CALLBACK = new DiffUtil.ItemCallback<NewsSection>() {
        @Override
        public boolean areItemsTheSame(@NonNull NewsSection oldItem, @NonNull NewsSection newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull NewsSection oldItem, @NonNull NewsSection newItem) {
            return oldItem.getDisplayName().equalsIgnoreCase(newItem.getDisplayName())
                    && oldItem.getStatus() != newItem.getStatus();
        }
    };

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_card_view, viewGroup, false);
        return new CategoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int i) {
        NewsSection newsSection = getItem(i);
        categoryViewHolder.tv_title.setText(newsSection.getDisplayName());
        if (newsSection.getStatus() > 0)
            categoryViewHolder.tv_status.setText(context.getString(R.string.following));
        else {
            categoryViewHolder.tv_status.setText(R.string.follow);
        }
    }


    public NewsSection getNewsSection(int position){
        return getItem(position);
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder{
//        @BindView(R.id.cardView)
//        CardView CardViewcv;

        @BindView(R.id.title)
        TextView tv_title;

        @BindView(R.id.tv_status)
        TextView tv_status;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
//            tv_title = itemView.findViewById(R.id.title);
//            tv_status = itemView.findViewById(R.id.tv_status);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v("onclick", getItem(getAdapterPosition()).getDisplayName());
                    if (onClickListner != null /*&& getItem(getAdapterPosition()).getStatus() > 0*/){
                        onClickListner.onClick(ActionOnClick.VIEW, getItem(getAdapterPosition()));
                    }
                }
            });
        }

        @OnClick(R.id.tv_status)
        public void SetStatus(){
            if (getItem(getAdapterPosition()).getStatus() > 0){
                getItem(getAdapterPosition()).setStatus(0);
                tv_status.setText(context.getString(R.string.follow));

            }else {
                getItem(getAdapterPosition()).setStatus(1);
                tv_status.setText(context.getString(R.string.following));
            }

            if (onClickListner != null){
                onClickListner.onClick(ActionOnClick.UPDATE, getItem(getAdapterPosition()));
            }
        }

    }


}
