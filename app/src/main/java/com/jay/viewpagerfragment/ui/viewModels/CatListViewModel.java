package com.jay.viewpagerfragment.ui.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.jay.viewpagerfragment.data.model.entities.NewsSection;
import com.jay.viewpagerfragment.data.repo.Repository;
import com.jay.viewpagerfragment.data.rest.ApiResponse;

import java.util.List;
import java.util.Observable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CatListViewModel extends ViewModel {

    private Repository repository;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();

    private final MutableLiveData<List<NewsSection>> newsSetcionData = new MutableLiveData<>();

    public CatListViewModel(Repository repository) {
        this.repository = repository;
    }


    public LiveData<List<NewsSection>> getSectionsFromLocal() {
        newsSetcionData.setValue(repository.getNewsSectionFromLocal());
        return newsSetcionData;
    }

    public LiveData<List<NewsSection>> getSections() {
        return repository.getNewsSections();
    }

    public Single<NewsSection> getFirstNewsSection(){
        return repository.getFirstNewsSection();
    }


    public boolean update(NewsSection newsSection) {

        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                repository.updateNewsSection(newsSection);
                if (newsSection.getStatus() <=0 ){
                    repository.clearArticles(newsSection.getId());
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        Log.v("Data added", "true");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
        /*repository.updateNewsSection(newsSection)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Integer>() {
                    @Override
                    public void onSuccess(Integer integer) {
                        Log.v("Updated", String.valueOf(integer));
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Updated",e.getMessage());
                    }
                });*/
        return true;
    }

    public boolean delete(NewsSection newsSection) {
        return true;
    }

    public boolean add(NewsSection newsSection) {
        return true;
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }
}
