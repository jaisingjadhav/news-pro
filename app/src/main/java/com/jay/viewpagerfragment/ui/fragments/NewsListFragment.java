package com.jay.viewpagerfragment.ui.fragments;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.jay.viewpagerfragment.R;
import com.jay.viewpagerfragment.data.model.entities.NewsSection;
import com.jay.viewpagerfragment.data.model.entities.Results;
import com.jay.viewpagerfragment.data.model.entities.NewsArticle;
import com.jay.viewpagerfragment.data.rest.ApiResponse;
import com.jay.viewpagerfragment.data.rest.Status;
import com.jay.viewpagerfragment.events.MessageEvent;
import com.jay.viewpagerfragment.events.NetworkStatus;
import com.jay.viewpagerfragment.ui.adapters.ActionOnClick;
import com.jay.viewpagerfragment.ui.adapters.ArticleAdapter;
import com.jay.viewpagerfragment.ui.adapters.OnClickListner;
import com.jay.viewpagerfragment.ui.viewModels.ArticleViewModel;
import com.jay.viewpagerfragment.ui.viewModels.ViewModelFactory;
import com.jay.viewpagerfragment.utils.UtilFunctions;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewsListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewsListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsListFragment extends Fragment implements OnClickListner {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String TAB_NAME = "tab";
    private static final String SECTION_NAME = "section_name";
    private static final String SECTION_ID = "section_id";

    // TODO: Rename and change types of parameters
    private String mtabName;
    private String mCategory;
    private int mCategoryId;

    private OnFragmentInteractionListener mListener;

    public NewsListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sectionName Parameter 1.
     * @param section Parameter 2.
     * @param section Parameter 2.
     * @return A new instance of fragment NewsListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewsListFragment newInstance(String sectionName, String section, int sectionId) {
        NewsListFragment fragment = new NewsListFragment();
        Bundle args = new Bundle();
        args.putString(TAB_NAME, sectionName);
        args.putString(SECTION_NAME, section);
        args.putInt(SECTION_ID, sectionId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //articleViewModel = ViewModelProviders.of(getActivity()).get(ArticleViewModel.class);
//        ((BaseApplication) getActivity().getApplication()).getAppComponent().doInjection(this);
//        articleViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ArticleViewModel.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //((BaseApplication) getActivity().getApplication()).getAppComponent().doInjection(getActivity());
        articleViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(ArticleViewModel.class);
        if (getArguments() != null) {
            mtabName = getArguments().getString(TAB_NAME);
            mCategory = getArguments().getString(SECTION_NAME);
            mCategoryId = getArguments().getInt(SECTION_ID, 1);
        }

        articleViewModel.listenArticles().observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                try {
                    consumeResponse(apiResponse);
                    if (apiResponse.data != null) {

                            /*if (apiResponse.status == Status.SUCCESS){
                                JsonArray jsonArray = ((JsonObject) apiResponse.data).getAsJsonArray("results");
                                if (jsonArray != null && jsonArray.size() >0){
                                    Gson gson = new Gson();
                                    Type type = new TypeToken<List<NewsArticle>>() {}.getType();
                                    List<NewsArticle> fromJson = gson.fromJson(jsonArray, type);
                                    articleViewModel.saveArticles(fromJson, mCategoryId);
                                }
                                Log.v("API Response Status", apiResponse.status.name());
                                Log.v("API Response", apiResponse.data.toString());
                            }*/
                    }else Log.v("API Response Status", apiResponse.status.name());
                }catch (Exception e){
                    Log.v("API Response Error", e.getMessage());
                }

            }
        });

    }

    @BindView(R.id.article_list_container)
    FrameLayout frameLayout;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.tv_message)
    TextView tv_emptyView;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    ViewModelFactory viewModelFactory;


    ArticleViewModel articleViewModel;
    ArticleAdapter articleAdapter;
    ProgressDialog progressDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        progressDialog = UtilFunctions.getProgressDialog(getActivity(), "Please wait...");
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);
        ButterKnife.bind(this, view);
        callAPI();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callAPI();
            }
        });

        setupRecyclerView();
        return view;
    }

    public void callAPI(){
        if (!UtilFunctions.checkInternetConnection(getActivity())){
            showNetworkSnack();
            if (progressDialog.isShowing())progressDialog.dismiss();
            if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
            return;
        }
        if (mCategory != null && articleViewModel != null ){
            articleViewModel.getAllArticles(mCategory+".json");
        }
    }

    private void consumeResponse(ApiResponse apiResponse) {

        switch (apiResponse.status) {

            case LOADING:
                progressDialog.show();
                break;

            case SUCCESS:
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                renderSuccessResponse(apiResponse.data);
                break;

            case ERROR:
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                try {
                    Snackbar snackbar = Snackbar
                            .make(frameLayout, "Error :"+apiResponse.error.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                } catch (Exception e){
                    Snackbar snackbar = Snackbar
                            .make(frameLayout, "Error", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

                break;

            default:
                break;
        }
    }

    /*
     * method to handle success response
     * */
    private void renderSuccessResponse(JsonElement response) {
        if (!response.isJsonNull()) {
            JsonArray jsonArray = ((JsonObject) response).getAsJsonArray("results");
            //JsonArray jsonArray = ((JsonObject) apiResponse.data).getAsJsonArray("results");
            if (jsonArray != null && jsonArray.size() >0){
                Gson gson = new Gson();
                Type type = new TypeToken<List<NewsArticle>>() {}.getType();
                List<NewsArticle> fromJson = gson.fromJson(jsonArray, type);
                articleViewModel.saveArticles(fromJson, mCategoryId);
            }
            //Log.v("API Response Status", response.status.name());
            Log.v("API Response", response.toString());
        } else {
            swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(),"", Toast.LENGTH_SHORT).show();
        }
    }
    private void setupRecyclerView(){
        articleAdapter = new ArticleAdapter(getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(articleAdapter);
        articleAdapter.setOnClickListner(this);

        articleViewModel.getNewsArticles(mCategoryId);

        articleViewModel.getNewsArticles(mCategoryId).observe(this, new Observer<List<NewsArticle>>() {
            @Override
            public void onChanged(@Nullable List<NewsArticle> newsArticles) {
                articleAdapter.submitList(newsArticles);

                if (newsArticles.size() > 0){
                    tv_emptyView.setVisibility(View.GONE);
                } else tv_emptyView.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public void onClick(ActionOnClick actionOnClick, Object object) {
        switch (actionOnClick){
            case SAVE:
                if (object instanceof NewsArticle) {

                }
                break;
            case VIEW:
                if (object instanceof NewsArticle) {
                    openWebPage(((NewsArticle) object).getShort_url());
                }
                break;
            case DELETE:
                if (object instanceof NewsArticle){
                    articleViewModel.clearArticles(((NewsArticle) object).getSection_id());
                }
                break;
            case UPDATE:
                if (object instanceof NewsArticle){

                }
                break;
        }
    }

    public void openWebPage(String url) {
        try {
            if (UtilFunctions.checkInternetConnection(getActivity())){
                Uri webpage = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    getActivity().startActivity(intent);
                }else{
                    //Page not found
                }
            }else {
               showNetworkSnack();
            }
        }catch (Exception e){
            Log.e("OpenWebPage", e.getMessage());
        }
    }

    private void showNetworkSnack(){
        Snackbar snackbar = Snackbar
                .make(frameLayout, getString(R.string.network_error), Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.dismiss, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        snackbar.show();
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }




}
