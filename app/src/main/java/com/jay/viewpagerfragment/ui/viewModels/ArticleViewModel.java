package com.jay.viewpagerfragment.ui.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.jay.viewpagerfragment.data.model.entities.NewsArticle;
import com.jay.viewpagerfragment.data.model.entities.Results;
import com.jay.viewpagerfragment.data.model.entities.NewsSection;
import com.jay.viewpagerfragment.data.repo.Repository;
import com.jay.viewpagerfragment.data.rest.ApiResponse;


import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

import io.reactivex.schedulers.Schedulers;

public class ArticleViewModel extends ViewModel {
    private Repository repository;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();
    private final LiveData<List<NewsArticle>> articleLiveData = new MutableLiveData<>();

    public ArticleViewModel(Repository repository) {
        this.repository = repository;
    }


    public List<NewsSection> getSectionsFromLocal() {
        return repository.getNewsSectionFromLocal();
    }

    public MutableLiveData<ApiResponse> listenArticles() {
        return responseLiveData;
    }
    public void getAllArticles(String section){
        disposables.add(repository.getAllArticle(section)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) -> responseLiveData.setValue(ApiResponse.loading()))
                .subscribe(
                        result -> responseLiveData.setValue(ApiResponse.success(result)),
                        throwable -> responseLiveData.setValue(ApiResponse.error(throwable))
                ));

    }

    public void saveArticles(List<NewsArticle> newsArticleList, int sectionId){
        repository.addNewsArticle(newsArticleList, sectionId);
    }
    public LiveData<List<NewsArticle>> getNewsArticles(int sectionId) {
        return repository.getNewsArticles(sectionId);
    }
    public void clearArticles(int section_id){
        repository.clearArticles(section_id);
    }
    @Override
    protected void onCleared() {
        disposables.clear();
    }
}
