package com.jay.viewpagerfragment.ui.activities;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jay.viewpagerfragment.R;
import com.jay.viewpagerfragment.base.BaseApplication;
import com.jay.viewpagerfragment.data.model.entities.NewsSection;
import com.jay.viewpagerfragment.events.MessageEvent;
import com.jay.viewpagerfragment.ui.adapters.ActionOnClick;
import com.jay.viewpagerfragment.ui.adapters.CategoryAdapter;
import com.jay.viewpagerfragment.ui.adapters.OnClickListner;
import com.jay.viewpagerfragment.ui.adapters.TabsAdapter;
import com.jay.viewpagerfragment.ui.fragments.NewsListFragment;
import com.jay.viewpagerfragment.ui.viewModels.ArticleViewModel;
import com.jay.viewpagerfragment.ui.viewModels.CatListViewModel;
import com.jay.viewpagerfragment.ui.viewModels.ViewModelFactory;
import com.jay.viewpagerfragment.utils.UtilFunctions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements NewsListFragment.OnFragmentInteractionListener, OnClickListner {

    @Inject
    ViewModelFactory viewModelFactory;

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    /* @BindView(R.id.tabs)
     TabLayout tabLayout;

     @BindView(R.id.viewPager)
     ViewPager viewPager;

     @BindView(R.id.fab_add_cat)
     FloatingActionButton fabButton;*/
    @BindView(R.id.fragment_container)
    FrameLayout fragment_container;

    TabsAdapter viewPagerAdapter;
    ProgressDialog progressDialog;

    @BindView(R.id.drawerLayout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.section_list)
    RecyclerView section_listView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_message)
    TextView tv_emptyView;

    CategoryAdapter categoryAdapter = null;

    CatListViewModel catListViewModel;
    ArticleViewModel articleViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        progressDialog = UtilFunctions.getProgressDialog(this, "Please wait...");
        ((BaseApplication) getApplication()).getAppComponent().doInjection(this);

        toolbar.setTitle(R.string.app_name);
        catListViewModel = ViewModelProviders.of(this, viewModelFactory).get(CatListViewModel.class);
        articleViewModel = ViewModelProviders.of(this, viewModelFactory).get(ArticleViewModel.class);


        setupActiveSection();


        categoryAdapter = new CategoryAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        section_listView.setLayoutManager(gridLayoutManager);
        section_listView.setAdapter(categoryAdapter);
        categoryAdapter.setOnClickListner(this);
        catListViewModel.getSections().observe(this, new Observer<List<NewsSection>>() {
            @Override
            public void onChanged(@Nullable List<NewsSection> newsSections) {
                categoryAdapter.submitList(newsSections);
                Log.v("DB Data", newsSections.toString());
            }
        });


        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);

        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();

    }

    private void setupActiveSection(){
        catListViewModel.getFirstNewsSection().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<NewsSection>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(NewsSection newsSection) {
                        if (newsSection != null && newsSection.getStatus() > 0){
                            showFragment(NewsListFragment.newInstance(newsSection.getDisplayName(), newsSection.getSectionName(), newsSection.getId()));
                            toolbar.setTitle(newsSection.getDisplayName());
                            tv_emptyView.setVisibility(View.GONE);
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, getString(R.string.empty_section), Snackbar.LENGTH_LONG);
                            snackbar.show();
                            mDrawerLayout.openDrawer(GravityCompat.START);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof NullPointerException) {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, getString(R.string.empty_section), Snackbar.LENGTH_LONG);
                            snackbar.show();
                            tv_emptyView.setVisibility(View.VISIBLE);
                            mDrawerLayout.openDrawer(GravityCompat.START);
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "ERROR "+e.getMessage(), Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                    }
                });
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    private void showFragment(Fragment fragment) {

        if (fragment != null) {
            if (tv_emptyView.getVisibility() == View.VISIBLE)
                tv_emptyView.setVisibility(View.GONE);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }

        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onClick(ActionOnClick actionOnClick, Object object) {
        switch (actionOnClick) {
            case SAVE:
                if (object instanceof NewsSection)
                    catListViewModel.update((NewsSection) object);
                break;
            case VIEW:
                if (object instanceof NewsSection) {
                    NewsSection newsSection = (NewsSection) object;
                    if (newsSection.getStatus() > 0) {
                        showFragment(NewsListFragment.newInstance(newsSection.getDisplayName(), newsSection.getSectionName(), newsSection.getId()));
                        toolbar.setTitle(newsSection.getDisplayName());
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, getString(R.string.follow_section), Snackbar.LENGTH_LONG);
                        snackbar.setAction(R.string.follow, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                newsSection.setStatus(1);
                                catListViewModel.update(newsSection);
                            }
                        });
                        snackbar.show();
                    }
                }
                break;
            case DELETE:
                if (object instanceof NewsSection)
                    catListViewModel.delete((NewsSection) object);
                break;
            case UPDATE:
                if (object instanceof NewsSection){
                    NewsSection newsSection = ((NewsSection)object);
                    catListViewModel.update(newsSection);
                    if (newsSection.getStatus() > 0){
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, getString(R.string.news_section, newsSection.getDisplayName(), newsSection.getDisplayName())
                                        , Snackbar.LENGTH_LONG);
                        snackbar.setAction(R.string.view_section, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toolbar.setTitle(newsSection.getDisplayName());
                                showFragment(NewsListFragment.newInstance(newsSection.getDisplayName(), newsSection.getSectionName(), newsSection.getId()));
                            }
                        });
                        snackbar.show();
                    } else {
                        //setupActiveSection();
                    }

                }

                break;
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    // This method will be called when a MessageEvent is posted (in the UI thread for Toast)
    @Subscribe
    public void onMessageEvent(MessageEvent event) {
        switch (event.networkStatus){
            case CONNECTED:
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, Html.fromHtml(getString(R.string.network_connected)), Snackbar.LENGTH_LONG);
                snackbar.show();
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if (fragment != null && fragment.isVisible()){
                    if (fragment instanceof NewsListFragment){
                        ((NewsListFragment) fragment).callAPI();
                    }
                }
                //callAPI();
                break;
            case DISCONNECTED:
                Snackbar snackbar1 = Snackbar
                        .make(coordinatorLayout, Html.fromHtml(getString(R.string.network_disconnected)), Snackbar.LENGTH_LONG);
                snackbar1.show();
                break;
        }
        //Toast.makeText(this, String.valueOf(event.networkStatus), Toast.LENGTH_SHORT).show();
    }
}
