package com.jay.viewpagerfragment.ui.adapters;

public enum ActionOnClick {
    DELETE,
    UPDATE,
    VIEW,
    SAVE
}
