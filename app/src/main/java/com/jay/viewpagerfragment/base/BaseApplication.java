package com.jay.viewpagerfragment.base;

import android.app.Application;
import android.content.Context;

import com.jay.viewpagerfragment.di.component.AppComponent;
import com.jay.viewpagerfragment.di.component.DaggerAppComponent;
import com.jay.viewpagerfragment.di.modules.AppModule;
import com.jay.viewpagerfragment.di.modules.UtilsModule;

public class BaseApplication extends Application {
    AppComponent appComponent;
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .utilsModule(new UtilsModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }
}
